//External imports
const express = require("express");
const router = express.Router();
const { Post } = require("../database");

router.post("/createpost", (req, res) => {
  const { username, image, description } = req.body;

  let post = new Post({
    username: username,
    image: image,
    description: description,
  });
  post.save(function (err, data) {
    if (err) {
      return res.status(500);
    } else {
      console.log("post created successfully");
      res.status(200).send("post created successfully");
    }
  });
});

module.exports = router;
