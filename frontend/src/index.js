// external dependencies
import React from "react";
import ReactDOM from "react-dom/client";
import { BrowserRouter, Routes, Route } from "react-router-dom";

// project internal dependencies
import App from "./App";
import { Login, Register, Createpost ,Home } from "./components";

// boilerplate code for web reporting
import reportWebVitals from "./reportWebVitals";

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <React.StrictMode>
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<App />} />
        <Route path="login" element={<Login />} />
        <Route path="register" element={<Register />} />
        <Route path="home" element={<Home />} />
        <Route path="createpost" element={<Createpost />} />
      </Routes>
    </BrowserRouter>
  </React.StrictMode>
);
reportWebVitals();
