import { useState } from "react";
import { validateEmail, validatePassword } from "../../validations";
import "./login.css";
import Logo from "../images/logo.jpg";

function Login() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const onChangeHandler = (ev) => {
    if (ev.target.name === "email") {
      setEmail(ev.target.value);
    } else if (ev.target.name === "password") {
      setPassword(ev.target.value);
    } else {
      console.log(ev.target.name + " did not match with anyone");
    }
  };

  const onClickHandler = (ev) => {
    if (!validateEmail(email) || !validatePassword(password)) {
      // invalid case
    } else {
      const data = { email, password };

      fetch("http://localhost:8000/login", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(data),
      })
        .then((response) => response.json())
        .then((data) => {
          console.log("Success:", data);
        })
        .catch((error) => {
          console.error("Error:", error);
        });
    }
  };

  return (
    <div className="container">
      <div className="card">
        <img className="logo" src={Logo} alt="" />
        <h1>Log in</h1>
        <p>Stay updated on your professional world</p>
        <input
          name="email"
          type="text"
          placeholder="enter email"
          value={email}
          onChange={onChangeHandler}
        ></input>
        <br />
        <input
          name="password"
          type="password"
          value={password}
          placeholder="enter password"
          onChange={onChangeHandler}
        ></input>
        <br />
        <a href="">
          <strong>Forgot password ?</strong>
        </a>
        <br />

        <button onClick={onClickHandler}>LOG IN</button>
        <br />
        <a href="">
          <strong>New to linkedin? Join now</strong>
        </a>
      </div>
    </div>
  );
}

export default Login;
