import { useState } from "react";
function CreatePost() {
  const [username, setUsername] = useState("");
  const [imageurl, setImageurl] = useState("");
  const [description, setDescription] = useState("");
  const onChangeHandler = (ev) => {
    if (ev.target.name === "username") {
      setUsername(ev.target.value);
    } else if (ev.target.name === "description") {
      setDescription(ev.target.value);
    } else if (ev.target.name === "imageurl") {
      setImageurl(ev.target.value);
    } else {
      console.log(ev.target.name + " username didnt match");
    }
  };
  const onClickHandler = (ev) => {
    const data = { username, imageurl, description };

    fetch("http://localhost:8000/createpost", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(data),
    })
      .then((response) => response.json())
      .then((data) => {
        console.log("Success:", data);
      })
      .catch((error) => {
        console.error("Error:", error);
      });
  };

  return (
    <>
      <div>
        <input
          name="username"
          type="text"
          placeholder="enter username"
          value={username}
          onChange={onChangeHandler}
        ></input>
        <input
          name="imageurl"
          value={imageurl}
          placeholder="enter image url"
          onChange={onChangeHandler}
          type="text"
        ></input>
        <input
          name="description"
          type="text"
          value={description}
          placeholder="enter postDescription"
          onChange={onChangeHandler}
        ></input>
        <button onClick={onClickHandler}>Post</button>
      </div>
    </>
  );
}

export default CreatePost;
