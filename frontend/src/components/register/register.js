import React from "react";
import Logo from "../images/logo.jpg";
import "./register.css";

class Register extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      username: "",
      username_err: "",
      email: "",
      password: "",
      pnumber:"",
      designation:""
    };

    this.onInputChanged = this.onInputChanged.bind(this);
    this.onButtonClick = this.onButtonClick.bind(this);
  }

  onInputChanged(et) {
    console.log(et.target.name, et.target.value);
    if (et.target.name === "username") {
      this.setState((prevState) => ({
        username: et.target.value,
      }));
    } else if (et.target.name === "password") {
      this.setState((prevState) => ({
        password: et.target.value,
      }));
    } else if (et.target.name === "email") {
      this.setState((prevState) => ({
        email: et.target.value,
      }));
    } else {
      console.log("unknown target name, dont know how to respond.");
    }
  }

  onButtonClick(et) {
    // validate it and do the below ONLY if validation passes

    const data = {
      username: this.state.username,
      password: this.state.password,
      email: this.state.email,
    };

    fetch("http://localhost:8000/register", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(data),
    })
      .then((response) => response.json())
      .then((data) => {
        console.log("Success:", data);
      })
      .catch((error) => {
        console.error("Error:", error);
      });
  }

  render() {
    return (
      <div className="container">
        <div className="card">
          <img className="logo" src={Logo} alt="" />
          <h1>Register</h1>
          <p>Stay updated on your professional world</p>
          <input
            name="email"
            type="text"
            placeholder="enter email"
            value={this.state.email}
            onChange={this.onInputChanged}
          ></input>
          <input
            name="username"
            type="text"
            placeholder="username"
            value={this.state.username}
            onChange={this.onInputChanged}
          ></input>

          <input
            name="password"
            type="password"
            placeholder="password"
            value={this.state.password}
            onChange={this.onInputChanged}
          ></input>
          <input
            name="pnumber"
            type="text"
            placeholder="phone number"
            value={this.state.pnumber}
            onChange={this.onInputChanged}
          ></input>

          <input
            name="designation"
            type="text"
            placeholder="designation"
            value={this.state.designation}
            onChange={this.onInputChanged}
          ></input>

          <a href="">
            <strong>Forgot password ?</strong>
          </a>
          <br />

          <button onClick={this.onButtonClick}>Register</button>
          <br />
          <a href="">
            <strong>New to linkedin? Join now</strong>
          </a>
        </div>
      </div>
    );
  }
}

export default Register;
{
  /* <div>
<input
  name="username"
  type="text"
  placeholder="enter username"
  value={this.state.username}
  onChange={this.onInputChanged}
></input>
<input
  name="email"
  type="text"
  placeholder="enter email"
  value={this.state.email}
  onChange={this.onInputChanged}
></input>
<input
  name="password"
  type="password"
  placeholder="enter password"
  value={this.state.password}
  onChange={this.onInputChanged}
></input>
<button onClick={this.onButtonClick}>NOKKU</button>
</div> */
}
